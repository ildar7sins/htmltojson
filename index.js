import init, { html_to_json, json_to_html, html_to_json_with_trim } from "./lib/html_to_json_2.js";

const NODES = {
    ELEMENT: 'Element',
    TEXT: 'Text',
    COMMENT: 'Comment',
    DOCTYPE: 'Doctype',
};

const htmlToJson = (content, trim = true) => {
    validateNonEmptyString(content);
    
    return trim ? html_to_json_with_trim(content) : html_to_json(content);
};

const jsonToHtml = (json) => {
    validateNonEmptyJsonString(json);
    
    return json_to_html(json);
};

// Вспомогательные функции для валидации
const validateNonEmptyString = (str) => {
    if (typeof str !== 'string' || str.trim() === '') {
        throw new Error('Content must be a non-empty string');
    }
};

const validateNonEmptyJsonString = (json) => {
    validateNonEmptyString(json);
    
    try {
        JSON.parse(json);
    } catch (e) {
        throw new Error('JSON string is not valid!');
    }
};

export { init, NODES, htmlToJson, jsonToHtml };
