use crate::editor::editor::Node;

#[allow(dead_code)]
pub fn nodes_to_html(nodes: Vec<Node>) -> String {
    nodes.into_iter().map(|node| node.to_html()).collect()
}