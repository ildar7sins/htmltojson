#[cfg(test)]
mod tests {
    use crate::{html_to_json, html_to_json_with_trim, try_parse};
    use crate::editor::editor::Node;
    use crate::models::json_obj::JsonObj;

    #[test]
    fn test_to_json() {
        let html = r#"<p class="test-class" id="test-id" disabled @model="user">Hello, </p><p>World!</p><p><br></p>"#;
        // let html = r#"<p><a href="/sdfsdf/sdfsdf">это </a>пример <a href="/dfgdfg/" target="_blank">простой ссылки</a></p><p>&nbsp;</p><blockquote><p>«Добейтесь того, чтобы все сотрудники вашей компании смотрели в одном направлении, и вы победите в любой отрасли, на любом рынке, всегда и везде».</p><p>Патрик Ленсиони, эксперт в области лидерства, командной работы и организационного развития</p></blockquote><p>&nbsp;</p><blockquote><p>«Добейтесь того, чтобы все сотрудники вашей компании смотрели в одном направлении, и вы победите в любой отрасли, на любом рынке, всегда и везде».</p></blockquote><p>&nbsp;</p><p>Ũ</p><p>±₽</p><h2 class="techtitle">&nbsp;</h2><h2 class="techtitle">Service Header (Заголовок технического блока) Почему-то работает в уроке, но не работает во введении.</h2><p><span class="text-tiny">'tiny'</span><br><span class="text-small">'small'</span><br>'default'<br><span class="text-big">'big'</span><br><span class="text-huge">'huge'</span></p><p class="sdfsdf">&nbsp;</p><p class="sdfsdf"><span style="font-family:Montserrat, sans-serif;">первый</span></p><p class="sdfsdf"><span style="font-family:Nunito, ui-sans-serif, Roboto, sans-serif;">второй</span></p><p class="sdfsdf"><span style="font-family:Georgia, Cambria, 'Times New Roman', Times, serif;">третий</span></p><p class="sdfsdf"><span style="font-family:Menlo, Monaco, Consolas, 'Liberation Mono', 'Courier New', monospace;">четвертый</span>&nbsp;</p><p class="sdfsdf"><span style="font-family:Roboto;">пятый</span></p><p class="sdfsdf">&nbsp;</p><p class="sdfsdf"><strong>bold</strong></p><p class="sdfsdf"><i>cursive</i></p><p class="sdfsdf"><u>underline</u></p><p class="sdfsdf"><s>striketrough</s></p><hr><p class="sdfsdf" style="text-align:right;">&nbsp;</p><p class="sdfsdf" style="text-align:right;">Это стандартные настройки (от Гоши):</p><h1 class="sdfsdf" style="color:red;">H1 заголовок первого уровня</h1><h2>H2 заголовок второго уровня</h2><h3>H3 заголовок третьего уровня</h3><h4><span style="background-color:hsl(30,75%,60%);color:hsl(150,75%,60%);">H4 заголовок четвертого уровня</span></h4><h5>H5 заголовок пятого уровня</h5><h6>H6 заголовок шестого уровня</h6><p>Стандартный параграф без редактирования стилей. Римский император Константин I Великий по достоинству оценил выгодное местоположение приморского Византия, расположенного на стыке Европы и Азии. Кроме того, на решение Константина повлияла неспокойная обстановка в самом Риме: недовольство знати и постоянные распри в борьбе за трон. Император хотел увенчать свою реформаторскую деятельность созданием нового административного центра огромной державы. Закладка города состоялась осенью 324 года, и Константин лично решил обозначить его границы.</p><ol><li>Нумерованный список 1</li><li>Нумерованный список 2<ol><li>ываываыва</li><li>ываываы</li><li>ываыыв</li></ol></li><li>Нумерованный список 3</li></ol><p>Нумерованный список с нуля&nbsp;</p><ol class="decimal-leading-zero"><li>Нумерованный список с нуля 1</li><li>Нумерованный список с нуля 2</li><li>Нумерованный список с нуля 3</li></ol><p>Маркированный список круги</p><ul style="list-style:disc;"><li>Маркированный список&nbsp; круги 1&nbsp;</li><li>Маркированный список круги 2<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul></li><li>Маркированный список круги 3</li></ul><p>&nbsp;</p><ul style="list-style:circle;"><li>Маркированный список&nbsp; круги 1&nbsp;</li><li>Маркированный список круги 2<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul></li></ul><p>Маркированный список квадраты</p><ul style="list-style:square;"><li>Маркированный список квадраты 1</li><li>Маркированный список квадраты 2</li><li>Маркированный список квадраты 3</li></ul><div class="raw-html-embed"><div id="anketolog-frame-801061"><user-service :model="user" @click.native="val => userUpdate(val)" /></div><script type="text/javascript">(function(d){ var u = 'https://anketolog.ru/api/v2/frame/js/801061?token=OlpmwO62'; var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = u; d.body.appendChild(s); })(document);</script></div><div class="raw-html-embed"><div id="tte-i-570626"></div><script src="https://www.testograf.ru/embed/embed.js?v=1705308200"></script><script>window.ttgrafSetEmbedParams({id: 570626,scrollToInvalid: true})</script></div><div class="raw-html-embed"><div id="tte-i-570624"></div><script src="https://www.testograf.ru/embed/embed.js?v=1705308200"></script><script>window.ttgrafSetEmbedParams({id: 570624,scrollToInvalid: true})</script><br><br/></div>"#;

        let nodes: Vec<Node> = try_parse(html);
        let json = JsonObj::to_json(nodes).unwrap_or(String::from(""));

        println!("test_to_json: {}", json);
    }

    #[test]
    fn test_to_html() {
        let json = r#"[{"obj_type":"Element","name":"p","attrs":[["@model","user"],["disabled",""],["id","test-id"],["class","test-class"]],"children":[{"obj_type":"Text","name":"Text","text":"Hello, "}],"id":"test-id","class":"test-class"},{"obj_type":"Element","name":"p","children":[{"obj_type":"Text","name":"Text","text":"World!"}]},{"obj_type":"Element","name":"p","children":[{"obj_type":"Element","name":"br"}]}]"#;
        let obj = JsonObj::to_html(String::from(json));

        println!("test_to_html: {:?}", obj);
    }

    #[test]
    fn test_to_json_and_back() {
        let html = r#"<iframe src="https://kinescope.io/200802464" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; encrypted-media;"></iframe>"#;

        let nodes: Vec<Node> = try_parse(html);
        let json = JsonObj::to_json(nodes).unwrap_or(String::from(""));
        println!("#1 test_to_json_and_back: {:?}", json);

        let new_html = JsonObj::to_html(String::from(json));
        println!("#2 test_to_json_and_back: {:?}", new_html);
    }

    #[test]
    fn test_func_to_json_with_trim() {
        let html = r#"  foo
            <section class="image-template">
    <div class="image-template-class-books" style=""

>
    </div>
    <pre>
            Пример текста с пробелами
        </pre>
    <h2 class="description-class" style="[object Object]">КНИГИ</h2>
    </section>"#;

        let not_trimmed = html_to_json(html);
        let trimmed = html_to_json_with_trim(html);

        println!("Check #1: {:?}", trimmed);
        println!("Check #2: {:?}", not_trimmed);
    }
}
