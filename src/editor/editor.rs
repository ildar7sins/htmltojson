use serde::{Deserialize, Serialize};
use crate::models::element::Element;
use crate::models::json_obj::JsonObj;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Doctype {
    Html,
    Xml { version: String, encoding: String },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Node {
    Element(Element),
    Text(String),
    Comment(String),
    Doctype(Doctype),
}

impl Node {
    pub fn is_element(&self) -> bool {
        matches!(self, Node::Element { .. })
    }

    #[deprecated(note = "Please use `is_element` instead")]
    pub fn into_element(self) -> Element {
        match self {
            Node::Element(element) => element,
            _ => panic!("{:?} is not an element", self),
        }
    }

    pub fn as_element(&self) -> Option<&Element> {
        match self {
            Node::Element(element) => Some(element),
            _ => None,
        }
    }

    pub fn as_element_mut(&mut self) -> Option<&mut Element> {
        match self {
            Node::Element(element) => Some(element),
            _ => None,
        }
    }

    pub fn new_element(name: &str, attrs: Vec<(&str, &str)>, children: Vec<Node>) -> Node {
        Element::new(
            name,
            attrs,
            children,
        )
            .into_node()
    }

    pub fn to_html(&self) -> String {
        match self {
            Node::Element(element) => element.to_html(),
            Node::Text(text) => text.clone(),
            Node::Comment(comment) => format!("<!-- {} -->", comment),
            Node::Doctype(doctype) => match doctype {
                Doctype::Html => "<!DOCTYPE html>".to_string(),
                Doctype::Xml { version, encoding } => {
                    format!("<!DOCTYPE xml VERSION=\"{}\" ENCODING=\"{}\">", version, encoding)
                }
            },
        }
    }
}

impl From<Node> for JsonObj {
    fn from(node: Node) -> Self {
        match node {
            Node::Element(element) => JsonObj {
                obj_type: "Element".to_string(),
                name: element.name,
                attrs: element.attrs,
                children: element.children.into_iter().map(JsonObj::from).collect(),
                id: element.id,
                class: element.class,
                ..Default::default()
            },
            Node::Text(text) => JsonObj {
                obj_type: "Text".to_string(),
                name: String::from("Text"),
                text: Some(text),
                ..Default::default()
            },
            Node::Comment(comment) => JsonObj {
                obj_type: "Comment".to_string(),
                name: String::from("Comment"),
                text: Some(comment),
                ..Default::default()
            },
            Node::Doctype(doctype) => JsonObj {
                obj_type: "Doctype".to_string(),
                name: String::from("Doctype"),
                text: Some(format!("{:?}", doctype)),
                ..Default::default()
            },
        }
    }
}

impl From<JsonObj> for Node {
    fn from(json_obj: JsonObj) -> Self {
        match json_obj.obj_type.as_str() {
            "Element" => Node::Element(Element::new(
                json_obj.name.as_str(),
                json_obj.attrs.iter().map(|(k, v)| (k.as_str(), v.as_str())).collect(),
                json_obj.children.into_iter().map(Node::from).collect(),
            )),
            "Text" => Node::Text(json_obj.text.expect("")),
            "Comment" => Node::Comment(json_obj.text.expect("")),
            "Doctype" => {
                // Ваша логика преобразования обратно в Doctype
                // В этом примере мы предполагаем, что Doctype необходимо преобразовать из строки
                Node::Doctype(Doctype::Html)
            }
            _ => unreachable!("Unexpected obj_type: {}", json_obj.obj_type),
        }
    }
}

impl From<Element> for Node {
    fn from(element: Element) -> Self {
        Node::Element(element)
    }
}
