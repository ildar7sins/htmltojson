mod compound;
mod simple;

use crate::models::element::Element;
use self::{compound::CompoundSelector, simple::SimpleSelector};

#[derive(Debug)]
pub struct Selector(Vec<CompoundSelector>);

impl Selector {
    pub fn matches(&self, element: &Element) -> bool {
        let element_classes = element
            .attrs
            .iter()
            .find(|(key, _)| key == "class")
            .map(|(_, v)| v.split(' ').map(|name| name.trim()).collect::<Vec<_>>());
        let element_id = element
            .attrs
            .iter()
            .find(|(key, _)| key == "id")
            .map(|(_, v)| v);

        self.0.iter().any(|compound_selector| {
            compound_selector
                .0
                .iter()
                .all(|simple_selector| match simple_selector {
                    SimpleSelector::Class(selector_class) => match &element_classes {
                        Some(element_classes) => element_classes
                            .iter()
                            .any(|element_class| element_class == selector_class),
                        None => false,
                    },
                    SimpleSelector::Id(selector_id) => match element_id {
                        Some(element_id) => element_id == selector_id,
                        None => false,
                    },
                    SimpleSelector::Tag(tag) => tag == &element.name,
                })
        })
    }
}

impl From<&str> for Selector {
    fn from(selector: &str) -> Self {
        Selector(selector.split(',').map(CompoundSelector::from).collect())
    }
}
