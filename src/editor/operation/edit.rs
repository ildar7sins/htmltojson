use super::Selector;
use crate::editor::editor::Node;
use crate::models::element::Element;

// Insert and remove elements by [`Selector`], and trim the DOM.
pub trait Editable {
    fn trim(&mut self) -> &mut Self;

    fn insert_to(&mut self, selector: &Selector, target: Node) -> &mut Self;

    fn remove_by(&mut self, selector: &Selector) -> &mut Self;

    fn replace_with(&mut self, selector: &Selector, f: fn(el: &Element) -> Node) -> &mut Self;

    #[allow(dead_code)]
    fn execute_for(&mut self, selector: &Selector, f: impl FnMut(&mut Element));
}

// We meed this function to allow the trait interface to use `impl FnMut(&mut Element)` instead of `&mut impl FnMut(&mut Element)`
fn nodes_execute_for_internal(
    nodes: &mut Vec<Node>,
    selector: &Selector,
    f: &mut impl FnMut(&mut Element),
) {
    for node in nodes {
        if let Some(element) = node.as_element_mut() {
            // Recursively traverse the descendants nodes
            element_execute_for_internal(element, selector, f);
        }
    }
}

// We meed this function to allow the trait interface to use `impl FnMut(&mut Element)` instead of `&mut impl FnMut(&mut Element)`
fn element_execute_for_internal(
    element: &mut Element,
    selector: &Selector,
    f: &mut impl FnMut(&mut Element),
) {
    if selector.matches(element) {
        f(element);
    }
    nodes_execute_for_internal(&mut element.children, selector, f);
}

impl Editable for Vec<Node> {
    fn trim(&mut self) -> &mut Self {
        self.retain(|node| match node {
            Node::Doctype(..) => true,
            Node::Comment(..) => false,
            Node::Text(text) => !text.trim().is_empty(),
            Node::Element { .. } => true,
        });
        for node in self.iter_mut() {
            if let Node::Element(el) = node {
                el.children.trim();
            }
        }
        self
    }

    fn insert_to(&mut self, selector: &Selector, target: Node) -> &mut Self {
        for node in self.iter_mut() {
            if let Node::Element(el) = node {
                el.children.insert_to(selector, target.clone());
                if selector.matches(&Element::new(
                    el.name.clone().as_str(),
                    el.attrs.clone().iter().map(|(k, v)| (k.as_str(), v.as_str())).collect(),
                    vec![]
                )) {
                    el.children.push(target.clone());
                }
            }
        }
        self
    }

    fn remove_by(&mut self, selector: &Selector) -> &mut Self {
        self.retain(|node| {
            if let Node::Element(el) = node {
                let element = Element::new(
                    el.name.clone().as_str(),
                    el.attrs.clone().iter().map(|(k, v)| (k.as_str(), v.as_str())).collect(),
                    vec![],
                );
                return !selector.matches(&element);
            }
            true
        });
        for node in self.iter_mut() {
            if let Node::Element(el) = node {
                el.remove_by(selector);
            }
        }
        self
    }

    fn replace_with(&mut self, selector: &Selector, f: fn(el: &Element) -> Node) -> &mut Self {
        for node in self.iter_mut() {
            if let Node::Element(ref mut el) = node {
                if selector.matches(el) {
                    *node = f(el);
                } else {
                    el.replace_with(selector, f);
                }
            }
        }
        self
    }

    fn execute_for(&mut self, selector: &Selector, mut f: impl FnMut(&mut Element)) {
        nodes_execute_for_internal(self, selector, &mut f);
    }
}

impl Editable for Element {
    fn trim(&mut self) -> &mut Self {
        self.children.trim();
        self
    }

    fn insert_to(&mut self, selector: &Selector, target: Node) -> &mut Self {
        self.children.insert_to(selector, target.clone());
        if selector.matches(self) {
            self.children.push(target);
        }
        self
    }

    fn remove_by(&mut self, selector: &Selector) -> &mut Self {
        self.children.remove_by(selector);
        self
    }

    fn replace_with(&mut self, selector: &Selector, f: fn(el: &Element) -> Node) -> &mut Self {
        self.children.replace_with(selector, f);
        self
    }

    fn execute_for(&mut self, selector: &Selector, mut f: impl FnMut(&mut Element)) {
        element_execute_for_internal(self, selector, &mut f);
    }
}
