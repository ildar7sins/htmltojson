use super::Selector;
use crate::editor::editor::Node;
use crate::models::element::Element;

pub trait Queryable {
    fn query(&self, selector: &Selector) -> Option<&Element>;

    fn query_all(&self, selector: &Selector) -> Vec<&Element>;

    fn query_mut(&mut self, selector: &Selector) -> Option<&mut Element>;
}

impl Queryable for Vec<Node> {
    fn query(&self, selector: &Selector) -> Option<&Element> {
        for node in self {
            if let Some(element) = node.as_element() {
                if let Some(elem) = element.query(selector) {
                    return Some(elem);
                }
            }
        }
        None
    }

    fn query_all(&self, selector: &Selector) -> Vec<&Element> {
        let mut elements = Vec::new();
        for node in self {
            if let Some(element) = node.as_element() {
                // Recursively traverse the descendants nodes
                let sub_elements = element.query_all(selector);
                elements.extend(sub_elements);
            }
        }
        elements
    }

    fn query_mut(&mut self, selector: &Selector) -> Option<&mut Element> {
        for node in self {
            if let Some(element) = node.as_element_mut() {
                if let Some(elem) = element.query_mut(selector) {
                    return Some(elem);
                }
            }
        }
        None
    }
}

impl Queryable for Element {
    fn query(&self, selector: &Selector) -> Option<&Element> {
        if selector.matches(self) {
            Some(self)
        } else {
            self.children.query(selector)
        }
    }

    fn query_all(&self, selector: &Selector) -> Vec<&Element> {
        let mut elements = self.children.query_all(selector);
        if selector.matches(self) {
            elements.push(self);
        }
        elements
    }

    fn query_mut(&mut self, selector: &Selector) -> Option<&mut Element> {
        if selector.matches(self) {
            Some(self)
        } else {
            self.children.query_mut(selector)
        }
    }
}

impl Queryable for Node {
    fn query(&self, selector: &Selector) -> Option<&Element> {
        if let Some(element) = self.as_element() {
            element.query(selector)
        } else {
            None
        }
    }

    fn query_all(&self, selector: &Selector) -> Vec<&Element> {
        if let Some(element) = self.as_element() {
            element.query_all(selector)
        } else {
            Vec::new()
        }
    }

    fn query_mut(&mut self, selector: &Selector) -> Option<&mut Element> {
        if let Some(element) = self.as_element_mut() {
            element.query_mut(selector)
        } else {
            None
        }
    }
}
