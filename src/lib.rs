mod editor;
mod models;
mod html_builder;

#[cfg(test)]
mod tests;

use wasm_bindgen::prelude::*;
use crate::editor::editor::Node;
pub use editor::parse::{try_parse};
use crate::models::json_obj::JsonObj;

#[wasm_bindgen]
pub fn html_to_json(html: &str) -> Result<std::string::String, JsValue> {
    let nodes: Vec<Node> = try_parse(html);
    let json = JsonObj::to_json(nodes);

    match json {
        Ok(res) => Ok(res),
        Err(err) => Err(JsValue::from_str(&format!("Error converting HTML to JSON: {}", err))),
    }
}

#[wasm_bindgen]
pub fn html_to_json_with_trim(html: &str) -> Result<std::string::String, JsValue> {
    let mut nodes: Vec<Node> = try_parse(html);
    let mut new_nodes: Vec<Node> = Vec::new();

    for node in &mut nodes {
        match node {
            // Вызов метода для удаления лишних текстовых узлов
            Node::Element(element) => {
                element.remove_whitespace_text_nodes();
                new_nodes.push(Node::Element(element.clone()));
            }

            // Добавляем только те Text, которые не пустые после trim
            Node::Text(text) => {
                let trimmed_text = text.trim();

                if !trimmed_text.is_empty() {
                    new_nodes.push(Node::Text(trimmed_text.to_string()));
                }
            }

            // Сохраняем комментарии и Doctype, игнорируя их
            Node::Comment(_) | Node::Doctype(_) => {
                new_nodes.push(node.clone());
            }
        };
    }

    let json = JsonObj::to_json(new_nodes);

    match json {
        Ok(res) => Ok(res),
        Err(err) => Err(JsValue::from_str(&format!("Error converting HTML to JSON: {}", err))),
    }
}

#[wasm_bindgen]
pub fn json_to_html(json: &str) -> Result<std::string::String, JsValue> {
    let html = JsonObj::to_html(json.to_string());

    match html {
        Ok(res) => Ok(res),
        Err(err) => Err(JsValue::from_str(&format!("Error converting JSON to HTML: {}", err))),
    }
}
