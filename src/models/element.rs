use crate::editor::editor::Node;
use serde::{Deserialize, Serialize};
use crate::models::tags::SINGLE_TAGS;

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Element {
    pub name: String,
    pub attrs: Vec<(String, String)>,
    pub children: Vec<Node>,
    pub id: Option<String>,
    pub class: Option<String>,
}

impl Element {
    pub fn new(name: &str, attrs: Vec<(&str, &str)>, children: Vec<Node>) -> Self {
        let mut class = None;
        let mut id = None;

        let attrs = attrs
            .into_iter()
            .rev()
            .map(|(k, v)| {
                if k == "class" {
                    class = Some(v.to_string());
                } else if k == "id" {
                    id = Some(v.to_string());
                }
                (k.to_string(), v.to_string())
            })
            .collect();

        Self {
            name: name.to_string(),
            attrs,
            children,
            class,
            id,
        }
    }
}

impl Element {
    pub fn remove_whitespace_text_nodes(&mut self) {
            // Создаем временный вектор для отфильтрованных детей
            let mut new_children = Vec::new();

            for child in &mut self.children {
                match child {
                    Node::Text(text) => {
                        let trimmed_text = text.trim();

                        // Добавляем только те Text, которые не пустые после trim
                        if !trimmed_text.is_empty() {
                            new_children.push(Node::Text(trimmed_text.to_string()));
                        }
                    }

                    Node::Element(element) => {
                        if element.name == "pre" {
                            // Если это <pre>, добавляем элемент без проверки детей
                            new_children.push(Node::Element(element.clone()));
                        } else {
                            // Рекурсивно удаляем пробелы у детей элемента
                            element.remove_whitespace_text_nodes();
                            new_children.push(Node::Element(element.clone()));
                        }
                    }

                    // Сохраняем комментарии и Doctype, игнорируя их
                    Node::Comment(_) | Node::Doctype(_) => {
                        new_children.push(child.clone());
                    }
                }
            }

            // Заменяем детей на отфильтрованные значения
            self.children = new_children;
    }

    pub fn into_node(self) -> Node {
        Node::Element(self)
    }

    // Функция для формирования HTML строки из текущего элемента и его дочерних элементов
    pub fn to_html(&self) -> String {
        let attributes = self
            .attrs
            .iter()
            .map(|(k, v)| {
                if v.is_empty() {
                    format!(" {}", k)
                } else {
                    format!(r#" {}="{}""#, k, v)
                }
            })
            .collect::<Vec<String>>()
            .join("");

        let children_html: String = self.children.iter().map(Node::to_html).collect();

        if SINGLE_TAGS.contains(&self.name.as_str()) {
            format!("<{}{}>", self.name, attributes)
        } else {
            format!("<{}{}>{}</{}>", self.name, attributes, children_html, self.name)
        }
    }
}
