use serde_json::Result;
use crate::editor::editor::Node;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct JsonObj {
    pub obj_type: String,
    pub name: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub text: Option<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub attrs: Vec<(String, String)>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub children: Vec<JsonObj>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub class: Option<String>,
}

impl JsonObj {
    pub fn to_json(nodes: Vec<Node>) -> Result<String> {
        let json_objs: Vec<JsonObj> = nodes.into_iter().map(|node| JsonObj::from(node)).collect();

        serde_json::to_string(&json_objs)
    }

    pub fn to_html(json: String) -> Result<String> {
        let json_objs: Vec<JsonObj> = serde_json::from_str(json.as_str())?;

        let nodes: Vec<Node> = json_objs.into_iter().map(Node::from).collect();

        let mut html = String::new();
        for node in nodes {
            html.push_str(&node.to_html());
        }

        Ok(html)
    }
}

impl Default for JsonObj {
    fn default() -> JsonObj {
        JsonObj {
            id: None,
            text: None,
            class: None,
            attrs: Vec::new(),
            name: String::new(),
            children: Vec::new(),
            obj_type: String::new(),
        }
    }
}
