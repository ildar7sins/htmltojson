use std::collections::HashSet;

lazy_static::lazy_static! {
    pub static ref SINGLE_TAGS: HashSet<&'static str> = {
        [
            "area", "base", "br", "col", "command",
            "embed", "hr", "img", "input", "keygen",
            "link", "meta", "param", "source", "track",
            "wbr"
        ].iter().cloned().collect()
    };
}
