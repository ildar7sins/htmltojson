/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function html_to_json(a: number, b: number, c: number): void;
export function html_to_json_with_trim(a: number, b: number, c: number): void;
export function json_to_html(a: number, b: number, c: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_malloc(a: number, b: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number, d: number): number;
export function __wbindgen_free(a: number, b: number, c: number): void;
